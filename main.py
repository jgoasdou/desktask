from email import parser
from functools import partial
from tkinter import *
import tkinter
from command import *
from config import config

global g
stop = 0

def reload(event, parent):
    global stop
    stop = 0
    print(stop)
    parent.destroy()


def close(event, parent):
    global stop
    stop = 1
    print(stop)
    parent.destroy()
    

def close_entry(user, data, event=None):
    for section in user:
        valider_entry(section, data)

def afficher_entry(entry, user, data, event=None):
    close_entry(user, data)
    print(entry)
    data['label_section_{}'.format(entry)].grid_forget()
    data['entry_section_{}'.format(entry)].grid( row = 0, columnspan= 4)
    data['entry_section_{}'.format(entry)].focus_set()


def valider_entry(entry, data, event=None):
    print(entry)
    data['entry_section_{}'.format(entry)].grid_forget() 
    data['label_section_{}'.format(entry)].grid( row = 0, columnspan= 4)

def save(event, data, root):
        for gchild in data:
            if gchild.startswith("var") :
                print("|||||||||||||||||||||-")
                pastsection = gchild[12:]
                newsection = data[gchild].get()
                print(pastsection)
                print(newsection)
                modifysection(pastsection, newsection)
        reload(event, root)
def initialisation():
    global g
    global stop
    debug = 1
    g = {}

    if verif_fichier("./user.ini") == FALSE :
        user = open("./user.ini", "x")
        user.close
    if verif_fichier("./config.ini") == FALSE :
        raise Exception('Il manque le fichier config.ini')

    parameters = config()
    defbg = parameters['background']
    deffont = parameters['font']
    deffg = parameters['foreground']
    deftitle = parameters['title']
    defminx = parameters['minsizex']
    defminy = parameters['minsizey']


   

    root = Tk() # Création de la fenêtre racine
    root.title(deftitle)
    root.minsize(defminx,defminy)
    # root.iconbitmap("chemin")
    root.config(background=defbg)
    # root.grid_rowconfigure(0, weight=1)
    root.grid_columnconfigure(0, weight=1)
    root.grid_columnconfigure(1, weight=1)

    # Configuration du menu 

    mainmenu = tkinter.Menu(root)
    menu_option = tkinter.Menu(mainmenu, tearoff=0)
    menu_option.add_command(label="Options")
    menu_option.add_command(label="Credits")
    menu_option.add_command(label="Quitter", command = lambda parent=root: 
                                                            close(parent))

    menu_categorie = tkinter.Menu(mainmenu, tearoff=0)
    menu_categorie.add_command(label="Add")
    menu_categorie.add_command(label="Delete")
    menu_categorie.add_command(label="Save", command=save)

    mainmenu.add_cascade(label="Options", menu=menu_option)
    mainmenu.add_cascade(label="Category", menu=menu_categorie)

    root.config(menu=mainmenu)

    # Configuration des binds

    root.bind('<Escape>', lambda event, parent=root:
                                close(event, parent))
    root.bind('<Control-s>', lambda event:
                                save(event, g, root))  
    root.bind('<Control-r>', lambda event, parent=root:
                                reload(event, parent))                            

    # addusersection("jeux")
    # adduserapp("jeux", "vs", "vs2")
    # modifysection("jeux", "new2")

    user = getusersections() # Recherche des catégories de l'utilisateur
    nombrecategorie = len(user)

    if nombrecategorie >0:
        for section in user:
            info = getsectioninfos(section)

            if debug == 1:
                print('   ')
                print('-------------------')
                print(section) #Nom de la catégorie
                print('-------------------')


            g['frame_{}'.format(section)] = Frame(root, bd=1, relief='raised', bg=defbg, padx= 20,height=100, width=160)
            # g['frame_{}'.format(section)].grid_propagate(0)
            g['frame_{}'.format(section)].grid(pady= 10,columnspan = 2)
            g['frame_{}'.format(section)].grid_columnconfigure(0, weight=1)
            g['frame_{}'.format(section)].grid_columnconfigure(1, weight=10)
            g['frame_{}'.format(section)].grid_columnconfigure(2, weight=10)
            

            # Création de la variable
            g['var_section_{}'.format(section)] = tkinter.StringVar()
            # Variable Set as Section
            g['var_section_{}'.format(section)].set(section)
            #Creation and display of Label Section
            g['label_section_{}'.format(section)] = tkinter.Label(g['frame_{}'.format(section)], textvariable=g['var_section_{}'.format(section)], font =(deffont, 15), bg=defbg, justify= CENTER , foreground=deffg)
            g['label_section_{}'.format(section)].grid(row = 0, columnspan = 4)
            #Button Ajout d'une app
            g['button_section_{}'.format(section)] = Button(g['frame_{}'.format(section)], text='+', command = partial(TopAddApp, root, section))
            g['button_section_{}'.format(section)].grid( row = 1, columnspan = 4)
            #Creation of Entry Section
            g['entry_section_{}'.format(section)] = tkinter.Entry(g['frame_{}'.format(section)],textvariable=g['var_section_{}'.format(section)], bg=defbg , justify = CENTER, width=20)
            #Bind of Label and Entry Section
            g['label_section_{}'.format(section)].bind('<Button-3>', partial(afficher_entry, section, user, g))
            g['entry_section_{}'.format(section)].bind('<Return>', partial(valider_entry, section, g))

            if debug == 1: 
                print(section)
                print( g['entry_section_{}'.format(section)] )
            
            n = 2
            for values in info:
                # label_list[]
                g['button_app_{}_{}'.format(section, values)] = Button(g['frame_{}'.format(section)], text='+', command = partial(startapp, r"" + info[values]))
                g['button_app_{}_{}'.format(section, values)].grid( row = n, column = 0)
                g['button_delete_{}_{}'.format(section, values)] = Button(g['frame_{}'.format(section)], text='-', command = partial(removeapp, section, values))
                g['button_delete_{}_{}'.format(section, values)].grid( row = n, column = 1)
                g['label_app_{}_{}'.format(section, values)] = Label(g['frame_{}'.format(section)], text=values, font =(deffont, 10), bg=defbg , foreground=deffg)
                g['label_app_{}_{}'.format(section, values)].grid( row = n,column = 2)
                g['label_path_{}_{}'.format(section, values)] = Label(g['frame_{}'.format(section)], text=info[values], font =(deffont, 10), bg=defbg , foreground=deffg)
                g['label_path_{}_{}'.format(section, values)].grid( row = n, column = 3)
                n = n+1

                if debug == 1:
                    print(values) # Nom de l'application
                    print(info[values]) #Path de l'application
                    print('-------------------')
    else:
        # Pas de catégorie
        print('')

    root.mainloop() # Lancement de la boucle principale

while stop != 1:
    initialisation()
    print(stop)