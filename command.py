from ast import Str
from tracemalloc import stop
from config import config
from functools import partial
import tkinter
from tkinter import *
from configparser import * 
from pathlib import Path
import subprocess
import os



parameters = config()
defbg = parameters['background']
deffont = parameters['font']
deffg = parameters['foreground']
deftitle = parameters['title']
defminx = parameters['minsizex']
defminy = parameters['minsizey']


# def touche(event):
#     t=event.keysym
#     print("Touche %s pressée" %t)

# root.bind('<Key>', touche)
def startapp(directory):
    os.startfile(directory)

def TopAddApp(root, section):
    TopAdd = Toplevel(root)
    TopAdd.title("Add App in " + section)
    TopAdd.geometry("200x200")
    Name = tkinter.StringVar()
    Path = tkinter.StringVar()
    Name_Label = Label(TopAdd,text ="Name :")
    Path_Label = Label(TopAdd, text = "Path :")
    Name_Entry = Entry(TopAdd, textvariable = Name)
    Path_Entry = Entry(TopAdd, textvariable = Path)
    Name_Label.grid(column=0, row=0)
    Name_Entry.grid(column=1, row=0)
    Path_Label.grid(column=0, row=1)
    Path_Entry.grid(column=1, row=1)

    Validate_Button = tkinter.Button(TopAdd, text="Ok", command = lambda : adduserapp( section, Name.get() , Path.get())) # adduserapp(section, name, path))
    Cancel_Button = tkinter.Button(TopAdd, text= "Cancel", command=TopAdd.destroy)
    Validate_Button.grid(column=0, row = 2)
    Cancel_Button.grid(column=1, row=2)

    TopAdd.mainloop()


def update_label(label, command, param=None):
    if command == 'text':
        label.config(text = param)
    if command == 'background':
        label.config(background = param)
    if command == 'padx':
        label.config(padx = param)
    if command == 'pady':
        label.config(pady = param)

def verif_fichier(filename):
    fileName = r"{0}".format(filename)
    fileObj = Path(fileName)
    return fileObj.is_file()

def getusersections(filename='user.ini'):

    # créer un analyseur
    parser = ConfigParser()
    # lire le fichier de configuration
    parser.read(filename)
    user = {}
    if parser.sections():
        user = parser.sections()
    else:
        # print("Aucune catégorie n'est crée") 
        print('')
    return user

def getsectioninfos(section ,filename='user.ini'):

    # créer un analyseur
    parser = ConfigParser()
    # lire le fichier de configuration
    parser.read(filename)

    # get section, par défaut à postgresql
    info = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            info[param[0]] = param[1]
    else:
        # print("Aucune catégorie n'est crée")
        print('')
    return info

# def getappsection(app)

def getpath(section, app, filename='user.ini'):
    # créer un analyseur
    parser = ConfigParser()
     # lire le fichier de configuration
    parser.read(filename)
    return parser.get(section, app)

def verify_section(section, filename='user.ini'):
    parser = ConfigParser()
    parser.read(filename)
    return parser.has_section(section)

def verify_app(section, app, filename='user.ini'):
    if verify_section(section) == FALSE:
        print("La section n'existe pas")
        return FALSE
    parser = ConfigParser()
    parser.read(filename)
    return parser.has_option(section, app)




def addusersection(section, filename='user.ini'):
    if verify_section(section) == TRUE:
        print('la section existe deja')
        return FALSE
    # créer un analyseur
    parser = ConfigParser()
    # lire le fichier de configuration
    parser.read(filename)

    parser.add_section(section)

    newini = open(filename, 'w')
    parser.write(newini)
    newini.close

def adduserapp(section, app, path, filename='user.ini'):
    print(section + "|" + app + "|" + path)
    if verify_section(section) == FALSE:
        print("La section n'existe pas !")
        return FALSE
    if verify_app(section, app) == TRUE:
        print("L'application existe deja!")
        return FALSE
    # créer un analyseur
    parser = ConfigParser()
     # lire le fichier de configuration
    parser.read(filename)
    
    parser.set(section, app, path)

    newini = open(filename, 'w')
    parser.write(newini)
    newini.close

def removeapp(section, app, filename='user.ini'):
    if verify_app(section, app) == FALSE:
        print("L'application n'existe pas !")
        return FALSE
    # créer un analyseur
    parser = ConfigParser()
     # lire le fichier de configuration
    parser.read(filename)
    parser.remove_option(section, app)

    newini = open(filename, 'w')
    parser.write(newini)
    newini.close

def removesection(section, filename='user.ini'):
    if verify_section(section) == FALSE:
        print("L'application n'existe pas !")
        return FALSE
    # créer un analyseur
    parser = ConfigParser()
     # lire le fichier de configuration
    parser.read(filename)
    parser.remove_section(section)

    newini = open(filename, 'w')
    parser.write(newini)
    newini.close

def modifypath(section, app, path, filename='user.ini'):
    if verify_section(section) == FALSE:
        print("La section n'existe pas !")
        return FALSE
    if verify_app(section, app) == FALSE:
        print("L'application n'existe pas !")
        return FALSE
    removeapp(section, app)
    adduserapp(section, app, path)

def modifyapp(section, app, newapp, filename='user.ini'):
    if verify_section(section) == FALSE:
        print("La section n'existe pas !")
        return FALSE
    if verify_app(section, app) == FALSE:
        print("L'application n'existe pas !")
        return FALSE
    if verify_app(section, newapp) == TRUE:
        print("Le nom est deja utilisé!")
        return FALSE

    path = getpath(section, app)
   
    removeapp(section, app)
    adduserapp(section, newapp, path)

def modifysection(section, newsection):
    if verify_section(section) == FALSE:
        print("La section n'existe pas !")
        return FALSE
    if verify_section(newsection) == TRUE:
        print("Le nom de la nouvelle section est deja prit !")
        return FALSE
    addusersection(newsection)
    sectionval = getsectioninfos(section)  
    for values in sectionval:
        adduserapp(newsection, values, sectionval[values])
    removesection(section)

def openapp(path):
    subprocess.run(path)

